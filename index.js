/**
 * @format
 */
import 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
// import App from './App';
import { name as appName } from './app.json';
import React from 'react'
import Router from './src/router/index'
import { NavigationContainer } from '@react-navigation/native'
import { Store, persistor } from './src/redux/store/store'
import { Provider } from 'react-redux'

import { PersistGate } from 'redux-persist/integration/react'


function App() {
    return (
        <Provider store={Store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <Router />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    )
}

AppRegistry.registerComponent(appName, () => App);
