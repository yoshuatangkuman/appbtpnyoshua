import React from 'react';

import {TouchableOpacity} from 'react-native'
import {launchImageLibrary} from 'react-native-image-picker'
import { View, Text } from 'react-native';
import storage from '@react-native-firebase/storage';

function App(){

  function chooseFile(){
    launchImageLibrary({}, async function(res){
      const asset = res.assets[0]
      const reference = storage().ref(`/photos/${asset.fileName}`);

      await reference.putFile(asset.uri);
      const url = await reference.getDownloadURL();

      console.log(url)
    })
  }
  
  return(
    <View>
      <TouchableOpacity onPress={chooseFile}>
        <Text>Upload</Text>
      </TouchableOpacity>
      </View>
    
  )
}

export default App