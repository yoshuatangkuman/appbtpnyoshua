import Axios from 'axios'

function getContacts() {
    // try{
    //     const {data} = await Axios.get('https://simple-contact-crud.herokuapp.com/contact')
    //     return data
    // }catch(error){
    //     throw error.response.data.message
    // }
    // return Axios.get('https://xxxxxx')
    return Axios.get('https://simple-contact-crud.herokuapp.com/contact')
}

async function getContactDetail(id) {
    try{
        const {data} = await Axios.get('https://simple-contact-crud.herokuapp.com/contact/'+id)
        return data
    }catch(error){
        throw error.response.data.message
    }
}

async function addContact(newcontact){
    console.log(newcontact, "newcontact===")

    try{
        const {data} = await Axios.post('https://simple-contact-crud.herokuapp.com/contact', newcontact)
        console.log(data, "data===")
        return data
    }catch(error){
        console.log(error, "error===")
        throw error.response.data.message
    }
}

async function updateContact(newcontact, id){
    console.log(newcontact, "newcontact===")
    console.log(id, "id===")

    try{
        const {data} = await Axios.put('https://simple-contact-crud.herokuapp.com/contact/'+id, newcontact)
        return data
    }catch(error){
        throw error.response.data.message
    }
}

const deleteContact = async (id) => {
    try{
        const {data} = await Axios.delete('https://simple-contact-crud.herokuapp.com/contact/'+id)
        return data
    }catch(error){
        throw error.response.data.message
    }
}

export { getContacts, addContact, updateContact, getContactDetail, deleteContact }