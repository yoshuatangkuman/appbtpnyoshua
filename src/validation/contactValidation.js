function contactValidation(firstname, lastname, age, imageurl){
    
    if(firstname.length==0){
        return "Firstname can't be empty"
    }else if(firstname.length>30){
        return "Firstname must be less than or equal to 30 characters"
    }else if(firstname.length<3){
        return "Firstname must be more than or equal to 3 characters"
    }else if(lastname.length>30){
        return "Lastname must be less than or equal to 30 characters"
    }else if(lastname.length<3){
        return "Lastname must be less than or equal to 3 characters"
    }else if(lastname.length==0){
        return "Lastname can't be empty"
    }else if(!age || age.length==0){
        return "Age can't be empty"
    }else if(age==0){
        return "Age must be larger than or equal to 1"
    }else if(age>100){
        return "Age must be less than or equal to 100"
    }else if(!imageurl || imageurl.length==0){
        return "Photo is not allowed to be empty"
    }else{
        return ""
    }
    
}

export default contactValidation