import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import * as screens from '../screens'

function Router() {

    const Stack = createStackNavigator()

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
            }}
        >
            <Stack.Screen name='contact_list' component={screens.ContactList}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen name='contact_add' component={screens.ContactAdd}
                options={{
                    title: 'Contact List',
                }}
            />

            <Stack.Screen name='contact_update' component={screens.ContactUpdate}
                options={{
                    title: 'Contact List',
                }}
            />

            <Stack.Screen name='contact_detail' component={screens.ContactDetail}
                options={{
                    title: 'Contact List',
                }}
            />

        </Stack.Navigator>
    )
}

export default Router