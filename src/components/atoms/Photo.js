import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function Photo({ imageurl, isLoading }) {

    return (
        <View style={ContactStyle.textInputFieldPhoto}>
            {isLoading &&

                <View style={ContactStyle.imageLoad}>
                    <ActivityIndicator size="large" color="#009dff" />
                </View>

            }
            {isLoading == false &&
                <Image
                    style={ContactStyle.image}
                    source={{
                        uri: imageurl
                    }}
                />
            }
        </View>
    )
}

export default Photo