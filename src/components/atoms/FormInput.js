import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormInput({ value, placeholder, onChangeText, keyboardType}) {
    return (
        <View style={ContactStyle.textInputField}>
            <TextInput value={value} placeholder={placeholder} onChangeText={onChangeText} keyboardType={keyboardType}/>
        </View>
    )
}

export default FormInput