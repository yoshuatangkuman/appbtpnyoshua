import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function ListButton({ onPress, btnStyle, label, btnTextStyle }) {
    return (
        <View style={ContactStyle.itemLayBtn}>
            <TouchableOpacity onPress={onPress} style={btnStyle} activeOpacity={0.8}>
                <Text style={btnTextStyle}>{label}</Text>
            </TouchableOpacity>
        </View>
    )
}

export default ListButton