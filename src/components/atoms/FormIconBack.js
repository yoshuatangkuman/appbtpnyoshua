import React from 'react'
import ContactStyle from '../../assets/styles/ContactStyle'
import Icon from 'react-native-vector-icons/FontAwesome'

function FormIconBack({ name, onPress }) {
    return (
        <Icon style={ContactStyle.iconForm} name={name} size={30} onPress={onPress}/>
    )
}

export default FormIconBack