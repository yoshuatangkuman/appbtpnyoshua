import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormButton({ onPress, disabled  }) {
    return (
        <TouchableOpacity onPress={onPress} disabled={disabled} style={ContactStyle.btn} activeOpacity={0.8}>
            <Text style={ContactStyle.textBtn}>SAVE</Text>
        </TouchableOpacity>
    )
}

export default FormButton