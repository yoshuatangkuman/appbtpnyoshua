import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormLabelPhoto({label}) {
    return (
        <Text style={ContactStyle.label2}>{label}</Text>
    )
}

export default FormLabelPhoto