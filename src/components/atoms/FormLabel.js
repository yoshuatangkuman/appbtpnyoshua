import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormLabel({label}) {
    return (
        <View style={ContactStyle.label}>
            <Text style={ContactStyle.label2}>{label}</Text>
        </View>
    )
}

export default FormLabel