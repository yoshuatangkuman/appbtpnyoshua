import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormLabelHeader({label}) {
    return (
        <Text style={ContactStyle.textTitlePageForm}>{label}</Text>
    )
}

export default FormLabelHeader