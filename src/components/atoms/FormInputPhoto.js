import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'

function FormInputPhoto({ onPress }) {
    return (
        <TouchableOpacity
            activeOpacity={0.5}
            style={ContactStyle.buttonStyle}
            onPress={onPress}>
            <Text style={ContactStyle.textStyle}>Choose Photo</Text>
        </TouchableOpacity>
    )
}

export default FormInputPhoto