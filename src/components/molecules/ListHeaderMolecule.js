import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'
import Icon from 'react-native-vector-icons/AntDesign'

function ListHeaderMolecule({ name, onPress, label }) {
    return (
        <View style={ContactStyle.topNavBarList}>
            <Text style={ContactStyle.textTitlePage}>{label}</Text>
            <Icon style={ContactStyle.iconRefresh} name={name} size={30} onPress={onPress} />
        </View>
    )
}

export default ListHeaderMolecule