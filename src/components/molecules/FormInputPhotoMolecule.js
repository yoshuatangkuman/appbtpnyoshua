import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'
import FormLabelPhoto from '../atoms/FormLabelPhoto'
import FormInputPhoto from '../atoms/FormInputPhoto'
import Photo from '../atoms/Photo'

function FormInputPhotoMolecule({ label, onPress, imageurl, isLoading}) {
    return (
        <View style={ContactStyle.fieldBox}>
            <View style={ContactStyle.labelPhoto}>
                <FormLabelPhoto label={label} />
                <FormInputPhoto onPress={onPress}/>
            </View>
            <Photo imageurl={imageurl} isLoading={isLoading}/>
        </View>
    )
}

export default FormInputPhotoMolecule