import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'
import FormIconBack from '../atoms/FormIconBack'
import FormLabelHeader from '../atoms/FormLabelHeader'

function FormHeaderMolecule({ name, onPress, label }) {
    return (
        <View style={ContactStyle.topNavBar}>
            <FormIconBack name={name} onPress={onPress}/>
            <FormLabelHeader label={label}/>
        </View>
    )
}

export default FormHeaderMolecule