import React from 'react'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import ContactStyle from '../../assets/styles/ContactStyle'
import FormInput from '../atoms/FormInput'
import FormLabel from '../atoms/FormLabel'

function FormInputMolecule({ label, value, placeholder, onChangeText, keyboardType}) {
    return (
        <View style={ContactStyle.fieldBox}>
            <FormLabel label={label} />
            <FormInput value={value} placeholder={placeholder} onChangeText={onChangeText} keyboardType={keyboardType}/>
        </View>
    )
}

export default FormInputMolecule