export const INITIATE_CONTACTS = "initiate_contact"
export const INITIATE_CONTACT_SUCCESS = "initiate_success_contact"
export const INITIATE_CONTACT_ERROR = "initiate_error_contact"

export const UPDATE_CONTACT = "update_contact"
export const UPDATE_CONTACT_SUCCESS = "update_contact_success"
export const UPDATE_CONTACT_ERROR = "update_contact_error"

export const DELETE_CONTACT = "DELETE_CONTACT"
export const DELETE_CONTACT_SUCCESS = "DELETE_CONTACT_SUCCESS"
export const DELETE_CONTACT_ERROR = "DELETE_CONTACT_ERROR"

export const ADD_CONTACT = "ADD_CONTACT"
export const ADD_CONTACT_SUCCESS = "ADD_CONTACT_SUCCESS"
export const ADD_CONTACT_ERROR = "ADD_CONTACT_ERROR"

function InitiateContacts(){
    return{
        type: INITIATE_CONTACTS
    }
}
function InitiateContactsSuccess(contacts){
    return{
        type: INITIATE_CONTACT_SUCCESS,
        data: contacts
    }
}
function InitiateContactsError(error){
    return{
        type: INITIATE_CONTACT_ERROR,
        error
    }
}

function AddContact(contact){
    return{
        type: ADD_CONTACT,
        data: {contact}
    }
}
function AddContactSuccess(){
    return{
        type: ADD_CONTACT_SUCCESS
    }
}
function AddContactError(error){
    return{
        type: ADD_CONTACT_ERROR,
        error
    }
}

function UpdateContact(newcontact, id){
    return{
        type: UPDATE_CONTACT,
        data: {
            newcontact,
            id
        }
    }
}
function UpdateContactSuccess(contact){
    return{
        type: UPDATE_CONTACT_SUCCESS,
        dataUpdate: contact
    }
}
function UpdateContactError(error){
    return{
        type: UPDATE_CONTACT_ERROR,
        error
    }
}

function DeleteContact(id){
    return{
        type: DELETE_CONTACT,
        data: {id}
    }
}
function DeleteContactSuccess(contact){
    return{
        type: DELETE_CONTACT_SUCCESS,
        dataDelete: contact
    }
}
function DeleteContactError(error){
    return{
        type: DELETE_CONTACT_ERROR,
        error
    }
}

function ClearContacts(){
    return{
        type: 'CLEAR_CONTACTS'
    }
}

export { InitiateContacts, InitiateContactsSuccess, InitiateContactsError, 
        UpdateContact, UpdateContactSuccess, UpdateContactError, 
        DeleteContact, DeleteContactSuccess, DeleteContactError,
        AddContact, AddContactSuccess, AddContactError,
        ClearContacts }