import { takeLatest, call, put, take } from 'redux-saga/effects'

import { INITIATE_CONTACTS, InitiateContactsSuccess, InitiateContactsError,
            UPDATE_CONTACT, UpdateContactSuccess, UpdateContactError,
            DELETE_CONTACT, DeleteContactSuccess, DeleteContactError,
            ADD_CONTACT, AddContactSuccess, AddContactError,
        } from "../actions/contact.action"

import {getContacts, updateContact, deleteContact, addContact} from "../../service/contactlist.service"

function* fetchContacts() {
    try {
        const contacts = yield call(getContacts)
        yield put(InitiateContactsSuccess(contacts.data.data))
    } catch (error) {
        yield put(InitiateContactsError(error.message))
    }
}

function* addContacts(action) {
    try {
        console.log('action===', action)
        console.log('action.data.contact===', action.data.contact)
        const contacts = yield call(addContact(action.data.contact))
        console.log('contacts===', contacts)
        yield put(AddContactSuccess())
    } catch (error) {
        console.log('error===', error)
        yield put(AddContactError(error))
    }
}

function* updateContacts(action) {
    try {
        const contacts = yield call(updateContact,action.data.newcontact, action.data.id)
        yield put(UpdateContactSuccess(contacts.data))
    } catch (error) {
        yield put(UpdateContactError(error))
    }
}

function* deleteContacts(action) {
    try {
        const contacts = yield call(deleteContact, action.data.id)
        yield put(DeleteContactSuccess(contacts.data))
    } catch (error) {
        yield put(DeleteContactError(error))
    }
}

function* watchContacts() {
    yield takeLatest(INITIATE_CONTACTS, fetchContacts)
    yield takeLatest(ADD_CONTACT, addContacts)  
    yield takeLatest(UPDATE_CONTACT, updateContacts)
    yield takeLatest(DELETE_CONTACT, deleteContacts)    
}

export default watchContacts