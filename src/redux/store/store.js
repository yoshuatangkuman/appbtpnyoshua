import { createStore, combineReducers, applyMiddleware } from 'redux'
import createSagaMiddleware from "redux-saga";
import watchContacts from "../sagas/contact.saga";
import ContactReducer from '../reducers/contact.reducer'
import { composeWithDevTools } from 'redux-devtools-extension';
import { persistStore, persistReducer } from 'redux-persist'
import storage from '@react-native-async-storage/async-storage'

const sagaMiddleware = createSagaMiddleware()

const rootReducers = combineReducers({
    // contactReducer: ContactReducer,
    ContactReducer
})

const persistConfig = {
    key: 'root',
    storage,
    whitelist : ['ContactReducer']
}

const Store = createStore(persistReducer(persistConfig, rootReducers), composeWithDevTools(applyMiddleware(sagaMiddleware)))
const persistor = persistStore(Store)

sagaMiddleware.run(watchContacts)

export {Store, persistor}