import {
    INITIATE_CONTACTS, INITIATE_CONTACT_SUCCESS, INITIATE_CONTACT_ERROR,
    UPDATE_CONTACT, UPDATE_CONTACT_SUCCESS, UPDATE_CONTACT_ERROR,
    DELETE_CONTACT, DELETE_CONTACT_ERROR, DELETE_CONTACT_SUCCESS,
    ADD_CONTACT, ADD_CONTACT_SUCCESS, ADD_CONTACT_ERROR
} from "../actions/contact.action"

const initState = {
    data: [],
    loading: false,
    error: undefined,
    dataUpdate: {},
    dataDelete: {},
    dataAdd: {}
}

const ContactReducer = (state = initState, action) => {
    switch (action.type) {
        case ADD_CONTACT:
            console.log(action, "action doang===")
            return {
                ...state, error: undefined, loading: true
            }
        case ADD_CONTACT_SUCCESS:
            console.log(action, "action success===")
            return {
                ...state, data: [], loading: false, error: undefined
            }
        case ADD_CONTACT_ERROR:
            console.log(action.error.message, "action error===")
            return {
                ...state, error: action.error.message, loading: false
            }
        case INITIATE_CONTACTS:
            return {
                ...state, loading: true, error: undefined
            }
        case INITIATE_CONTACT_SUCCESS:
            return {
                ...state, data: action.data, loading: false, error: undefined
            }
        case INITIATE_CONTACT_ERROR:
            return {
                ...state, error: action.error.message, loading: false
            }
        case 'CLEAR_CONTACTS':
            return {
                ...state, data: [], error: undefined, loading: true
            }
        case UPDATE_CONTACT:
            return {
                ...state, error: undefined, loading: true
            }
        case UPDATE_CONTACT_SUCCESS:
            const Contacts = [...state.data]
            const newContacts = Contacts.map(contact => {
                if (contact.id === action.dataUpdate.id) {
                    contact.age = action.dataUpdate.age
                    contact.firstName = action.dataUpdate.firstName
                    contact.lastName = action.dataUpdate.lastName
                    contact.photo = action.dataUpdate.photo
                }
                return contact
            })
            return {
                ...state, data: newContacts, error: undefined, loading: false
            }
        case UPDATE_CONTACT_ERROR:
            return {
                ...state, error: action.error, loading: false
            }
        case DELETE_CONTACT:
            return {
                ...state, error: undefined
            }
        case DELETE_CONTACT_SUCCESS:
            const contactsBefore = [...state.data]
            const contactsAfter = contactsBefore.filter(contact => contact.id != action.dataDelete.id)
            return {
                ...state, data: contactsAfter, error: undefined
            }
        case DELETE_CONTACT_ERROR:
            return {
                ...state, error: action.error
            }
        default:
            return state
    }
}

export default ContactReducer