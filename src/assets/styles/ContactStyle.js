import { StyleSheet } from "react-native"

const ContactStyle = StyleSheet.create({
    topNavBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        alignContent: 'center',
        width: '100%',
        height:80
    },
    mainForm: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    loginFormBox: {

        height: 600,
        width: '100%',
        padding: 4,
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: 'white',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 24,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 100,
        height: 40
    },
    buttonStyle: {
        // alignSelf:'flex-start',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#009dff',
        padding: 5,
        // marginVertical: 10,
        width: 110,
        borderRadius: 12,
        elevation: 8,
        height: 30,
        marginLeft: 20
    },
    textBtn: {
        color: 'white',
        fontWeight: 'bold'
    },
    fieldBox: {
        alignItems: 'flex-start',
        width: 300
    },
    label: {
        marginVertical: 12,

    },
    labelPhoto: {
        marginVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    label2: {
        color: '#009dff',
        fontWeight: 'bold',
        fontSize: 15
    },
    textInputField: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'flex-start',
        width: 300,
        borderWidth: 1,
        borderColor: '#009dff',
        borderRadius: 12
    },
    textInputFieldPhoto: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'center',
        width: 300,
    },
    photoBox: {
        alignSelf:'center',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        borderWidth:1
    }, 
    labelLoad: {
        width: 300,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    textStyle: {
        color: 'white'
    },
    image: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center'
    },
    imageLoad: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center',
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center'
    },
    icon: {
        color: 'white',
        alignSelf: 'center',
    },
    iconForm: {
        color: 'white',
        alignSelf: 'center',
        marginLeft:20,
        marginRight:99,
    },
    iconRefresh: {
        color: 'white',
        alignSelf: 'center',
        marginRight: 16
    },
    itemList: {
        borderWidth: 1,
        borderColor: '#009dff',
        marginBottom: 24,
        marginHorizontal: 24,
        padding: 16,
        color: '#8F98A9',
        borderRadius: 12,
        backgroundColor: '#F8FCFF',
        flexDirection: 'row',
        alignItems: 'center',
        maxWidth: 350,
        alignSelf: 'center'
    },
    leftList: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        width: '70%'
    },
    rightList: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        alignSelf: 'flex-start',
        width: '30%',
    },
    imageList: {
        width: 100,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    main: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: '100%'
    },
    topNavBarList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    textTitlePage: {
        color: 'white',
        margin: 24,
        fontSize: 24,
        fontWeight: 'bold',
    },
    textTitlePageForm: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    mainBox: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24,
    },
    textMain: {
        color: '#009dff',
    },
    textHeader: {
        color: '#009dff',
        fontWeight: 'bold'
    },
    itemContent: {
        alignContent: 'flex-start',
        marginTop: 10
    },
    itemLayBtn: {
        alignContent: 'flex-end',
        marginBottom: 20
    },
    btnAdd: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 20,
        marginVertical: 12,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 50,
        width: 60,
        height: 60,
        position: 'absolute',
        bottom: 10,
        right: 0
    },
    textBtnAdd: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    btnEdit: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#009dff",
        marginRight: 12,
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnEditText: {
        fontSize: 13,
        color: "white",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
    btnDel: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#730202",
        marginRight: 12,
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnDelText: {
        fontSize: 13,
        color: "white",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
})

export default ContactStyle