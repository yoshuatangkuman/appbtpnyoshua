import React, { useCallback, useEffect, useState } from 'react'
import { useRoute, useNavigation, useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux'
import ContactReducer from '../redux/reducers/contact.reducer'
import { ClearContacts, InitiateContacts } from '../redux/actions/contact.action'
import { View, FlatList, Text, StyleSheet, Button, TouchableOpacity, Image, Alert, ToastAndroid, ActivityIndicator } from 'react-native'
import { getContacts, deleteContact } from '../service/contactlist.service'
import { DeleteContact } from '../redux/actions/contact.action';
import Icon from 'react-native-vector-icons/AntDesign'
import ListHeaderMolecule from '../components/molecules/ListHeaderMolecule';
import ListButton from '../components/atoms/ListButton';
import ContactStyle from '../assets/styles/ContactStyle';

function ContactList() {

    const navigate = useNavigation()
    const route = useRoute()
    const dispatch = useDispatch()
    // const state = useSelector((state) => state.contactReducer)

    const state = useSelector(state => state.ContactReducer.data)
    const error = useSelector(state => state.ContactReducer.error)
    const loading = useSelector(state => state.ContactReducer.loading)

    const [isError, setIsError] = useState(false)
    const [isLoading, setIsLoading] = useState(true)


    useFocusEffect(useCallback(() => {
        if (state.length === 0) {
            dispatch(InitiateContacts())
        }
    }, [state]))

    useEffect(()=>{
        if(isError){
            if(error){
                ToastAndroid.show(error, ToastAndroid.LONG)
            }
        }else{
            setIsError(true)
        }
    }, [error])

    function refresh() {
        dispatch(ClearContacts())
        // setIsLoading(true)
    }


    function navigatePageAdd() {
        navigate.navigate("contact_add")
    }

    function navigatePageUpdate(id, firstName, lastName, age, photo) {
        const contact = {
            id: id,
            firstName: firstName,
            lastName: lastName,
            age: age,
            photo: photo
        }

        navigate.navigate("contact_update", contact)
    }

    function navigatePageDetail(id) {
        const contact = {
            id: id
        }

        navigate.navigate("contact_detail", contact)
    }

    function deleteContactPrompt(id) {
        Alert.alert(
            "Are you sure you want to delete contact?",
            "",
            [
                {
                    text: "Cancel",
                    style: "cancel"
                },
                {
                    text: "OK",
                    onPress: () => refreshList(id)
                }
            ])
    }

    function refreshList(id) {
        dispatch(DeleteContact(id))
    }


    return (
        <View style={styles.main}>
            {loading && <ActivityIndicator style={{ alignSelf: 'center' }} size="large" color="white" />}
            {loading == false &&
            
                <View style={styles.main}>
                    <ListHeaderMolecule label="Contact List" name="reload1" onPress={refresh} />
                    <View style={styles.mainBox}>
                        <FlatList
                            data={state}
                            renderItem={({ item }) =>
                                <View style={styles.itemList}>
                                    <View style={styles.leftList}>
                                        <TouchableOpacity onPress={() => navigatePageDetail(item.id)}>
                                            <View>
                                                {item.photo != "N/A"
                                                    ?
                                                    <Image
                                                        style={styles.imageList}
                                                        source={{
                                                            uri: item.photo,
                                                        }}
                                                    />
                                                    :
                                                    <Image
                                                        style={styles.imageList}
                                                        source={require('../images/noimage.png')}
                                                    />

                                                }
                                            </View>
                                            <View style={styles.itemContent}>
                                                <Text style={styles.textHeader}>Name : {item.firstName} {item.lastName}</Text>
                                                <Text style={styles.textMain}>Age :{item.age}</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={styles.rightList}>
                                        <ListButton onPress={() => navigatePageUpdate(item.id, item.firstName, item.lastName, item.age, item.photo)} btnStyle={ContactStyle.btnEdit} label="EDIT" btnTextStyle={ContactStyle.btnEditText}/>
                                        <ListButton onPress={() => deleteContactPrompt(item.id)} btnStyle={ContactStyle.btnDel} label="DELETE" btnTextStyle={ContactStyle.btnDelText}/>
                                    </View>
                                </View>
                            }
                            keyExtractor={(item) => item.id}
                        />
                        <TouchableOpacity onPress={navigatePageAdd} style={styles.btnAdd} activeOpacity={0.8}>
                            <Icon style={styles.icon} name="plus" size={30} />
                        </TouchableOpacity>
                    </View>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    icon: {
        color: 'white',
        alignSelf: 'center',
    },
    iconRefresh: {
        color: 'white',
        alignSelf: 'center',
        marginRight: 16
    },
    itemList: {
        borderWidth: 1,
        borderColor: '#009dff',
        marginBottom: 24,
        marginHorizontal: 24,
        padding: 16,
        color: '#8F98A9',
        borderRadius: 12,
        backgroundColor: '#F8FCFF',
        flexDirection: 'row',
        alignItems: 'center',
        maxWidth: 350,
        alignSelf: 'center'
    },
    leftList: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        alignSelf: 'flex-start',
        width: '70%'
    },
    rightList: {
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        alignSelf: 'flex-start',
        width: '30%',
    },
    imageList: {
        width: 100,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    main: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: '100%'
    },
    topNavBarList: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    textTitlePage: {
        color: 'white',
        margin: 24,
        fontSize: 24,
        fontWeight: 'bold',
    },
    mainBox: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24,
    },
    textMain: {
        color: '#009dff',
    },
    textHeader: {
        color: '#009dff',
        fontWeight: 'bold'
    },
    itemContent: {
        alignContent: 'flex-start',
        marginTop: 10
    },
    itemLayBtn: {
        alignContent: 'flex-end',
        marginBottom: 20
    },
    btnAdd: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end',
        marginRight: 20,
        marginVertical: 12,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 50,
        width: 60,
        height: 60,
        position: 'absolute',
        bottom: 10,
        right: 0
    },
    textBtnAdd: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    btnEdit: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#009dff",
        marginRight: 12,
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnEditText: {
        fontSize: 13,
        color: "white",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
    btnDel: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#730202",
        marginRight: 12,
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnDelText: {
        fontSize: 13,
        color: "white",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
})

export default ContactList