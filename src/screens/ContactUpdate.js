import React, { useEffect } from 'react'
import { useRoute, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux'
import { ClearContacts, UpdateContact } from '../redux/actions/contact.action'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { updateContact } from '../service/contactlist.service'
import { launchImageLibrary } from 'react-native-image-picker'
import storage from '@react-native-firebase/storage';
import contactValidation from '../validation/contactValidation';
import { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome'
import Photo from '../components/atoms/Photo'
import ContactReducer from '../redux/reducers/contact.reducer'
import FormInputMolecule from '../components/molecules/FormInputMolecule';
import FormInputPhotoMolecule from '../components/molecules/FormInputPhotoMolecule';
import FormButton from '../components/atoms/FormButton';
import ContactStyle from '../assets/styles/ContactStyle';
import FormHeaderMolecule from '../components/molecules/FormHeaderMolecule';

function ContactUpdate() {

    const dispatch = useDispatch()
    const navigate = useNavigation()
    const route = useRoute()

    const [id, setId] = useState('')
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [age, setAge] = useState(0)
    const [imageurl, setImageurl] = useState()
    const [ageText, setAgeText] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [isError, setIsError] = useState(false)

    const error = useSelector(state => state.ContactReducer.error)
    const loading = useSelector(state => state.ContactReducer.loading)

    useEffect(() => {
        setId(route.params.id)
        setFirstname(route.params.firstName)
        setLastname(route.params.lastName)
        setAge(route.params.age)
        setImageurl(route.params.photo)
        setAgeText(route.params.age.toString())
    }, [])

    useEffect(()=>{
        if(isError){
            if(error){
                setIsLoading(false)
                ToastAndroid.show(error, ToastAndroid.LONG)
            }else{
                ToastAndroid.show("Contact Updated", ToastAndroid.LONG)
                navigate.navigate("contact_list")
            }
        }else{
            setIsError(true)
        }
    }, [error, loading])

    function handleFirstName(text) {
        setFirstname(text)
    }

    function handleLastName(text) {
        setLastname(text)
    }

    function handleAge(text) {
        text = text.replace(/[^0-9]/g, '')
        setAgeText(text)
        setAge(parseInt(text))
    }

    function chooseFile() {
        setIsLoading(true)
        launchImageLibrary({}, async function (res) {
            if (res && res.assets) {
                const asset = res.assets[0]
                const reference = storage().ref(`/photos/${asset.fileName}`);

                await reference.putFile(asset.uri);
                const url = await reference.getDownloadURL();
                setImageurl(url)
                setIsLoading(false)
            } else {
                if (imageurl == "") {
                    setImageurl("N/A")
                }
                setIsLoading(false)
            }
        })
    }


    function validation() {
        setIsLoading(true)
        let messageError = contactValidation(firstname, lastname, age, imageurl)
        if (messageError == '') {
            navigatePageList()
        } else {
            ToastAndroid.show(messageError, ToastAndroid.LONG);
            setIsLoading(false)
        }
    }

    function navigatePageList() {
        const newContact = {
            firstName: firstname,
            lastName: lastname,
            age: age,
            photo: imageurl
        }

        dispatch(UpdateContact(newContact, id))
        // if(check){
        //     setIsLoading(false)
            
        // }else{
        //     ToastAndroid.show("Contact Updated", ToastAndroid.LONG)
        //     navigate.navigate("contact_list")
        // }
    }

    function navigatePageBack() {
        navigate.navigate("contact_list")
    }

    return (
        // <ScrollView>
        //     <View style={styles.main}>
        //         <View style={styles.topNavBar}>
        //             <Icon style={styles.icon} name="angle-left" size={30} onPress={navigatePageBack} />
        //             <Text style={styles.textTitlePage}>Update Contact</Text>
        //         </View>
        //         <View style={styles.loginFormBox}>
        //             <View style={styles.fieldBox}>
        //                 <View style={styles.label}>
        //                     <Text style={styles.label2}>First Name</Text>
        //                 </View>
        //                 <View style={styles.textInputField}>
        //                     <TextInput value={firstname} placeholder='Insert First Name' onChangeText={handleFirstName} />
        //                 </View>
        //             </View>
        //             <View style={styles.fieldBox}>
        //                 <View style={styles.label}>
        //                     <Text style={styles.label2}>Last Name</Text>
        //                 </View>
        //                 <View style={styles.textInputField}>
        //                     <TextInput value={lastname} placeholder='Insert Last Name' onChangeText={handleLastName} />
        //                 </View>
        //             </View>
        //             <View style={styles.fieldBox}>
        //                 <View style={styles.label}>
        //                     <Text style={styles.label2}>Age</Text>
        //                 </View>
        //                 <View style={styles.textInputField}>
        //                     <TextInput keyboardType='numeric' value={ageText} placeholder='Insert Age' onChangeText={handleAge} />
        //                 </View>
        //             </View>
        //             <View style={styles.fieldBox}>
        //                 <View style={styles.labelPhoto}>
        //                     <Text style={styles.label2}>Photo</Text>
        //                     <TouchableOpacity
        //                         activeOpacity={0.5}
        //                         style={styles.buttonStyle}
        //                         onPress={() => chooseFile('photo')}>
        //                         <Text style={styles.textStyle}>Choose Photo</Text>
        //                     </TouchableOpacity>
        //                 </View>
        //                 <Photo imageurl={imageurl} isLoading={isLoading}/>
        //                 {/* <View style={styles.textInputFieldPhoto}>
        //                     {isLoading &&
        //                         <View style={styles.imageLoad}>
        //                             <ActivityIndicator size="large" color="#009dff" />
        //                         </View>
        //                     }
        //                     {isLoading == false &&
        //                         <Image
        //                             style={styles.image}
        //                             source={{
        //                                 uri: imageurl
        //                             }}
        //                         />
        //                     }
        //                 </View> */}
        //             </View>
        //             {isLoading &&
        //                 <TouchableOpacity onPress={() => validation()} disabled={true} style={styles.btn} activeOpacity={0.8}>
        //                     <Text style={styles.textBtn}>SAVE</Text>
        //                 </TouchableOpacity>
        //             }
        //             {isLoading == false &&
        //                 <TouchableOpacity onPress={() => validation()} style={styles.btn} activeOpacity={0.8}>
        //                     <Text style={styles.textBtn}>SAVE</Text>
        //                 </TouchableOpacity>
        //             }
        //         </View>
        //     </View>
        // </ScrollView>
        <ScrollView>
            <View style={ContactStyle.mainForm}>
                <FormHeaderMolecule label="Update Contact" name="angle-left" onPress={navigatePageBack}/>
                <View style={ContactStyle.loginFormBox}>
                    <FormInputMolecule label="First Name" value={firstname} placeholder="Insert First Name" onChangeText={handleFirstName}/>
                    <FormInputMolecule label="Last Name" value={lastname} placeholder="Insert Last Name" onChangeText={handleLastName}/>
                    <FormInputMolecule label="Age" keyboardType='numeric' value={ageText} placeholder="Insert Age" onChangeText={handleAge}/>
                    <FormInputPhotoMolecule label="Photo" onPress={() => chooseFile('photo')} imageurl={imageurl} isLoading={isLoading} />
                    {isLoading &&
                        <FormButton onPress={() => validation()} disabled={true}/>
                    }
                    {isLoading == false &&
                        <FormButton onPress={() => validation()} disabled={false}/>
                    }
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    photoBox: {
        alignSelf: 'center'
    },
    labelLoad: {
        width: 300,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    textStyle: {
        color: 'white'
    },
    imageLoad: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center',
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center'
    },
    image: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center'
    },
    main: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    topNavBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        alignContent: 'center',
        width: '100%',
        height: 80
    },
    textTitlePage: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    icon: {
        color: 'white',
        alignSelf: 'center',
        marginLeft: 20,
        marginRight: 86,
    },
    mainBox: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24
    },
    loginFormBox: {

        height: 600,
        width: '100%',
        padding: 4,
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: 'white',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    fieldBox: {
        alignItems: 'flex-start',
        width: 300
    },
    label: {
        marginVertical: 12,

    },
    labelPhoto: {
        marginVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    label2: {
        color: '#009dff',
        fontWeight: 'bold',
        fontSize: 15
    },
    textInputField: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'flex-start',
        width: 300,
        borderWidth: 1,
        borderColor: '#009dff',
        borderRadius: 12
    },
    textInputFieldPhoto: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'center',
        width: 300,
    },
    textMain: {
        color: '#8F98A9',
    },
    textHeader: {
        color: '#009dff',
        fontWeight: 'bold'
    },
    itemContent: {
        alignContent: 'flex-start',
    },
    itemList: {
        marginBottom: 24,
        marginHorizontal: 24,
        padding: 16,
        color: '#8F98A9',
        borderRadius: 12,
        backgroundColor: '#FAFAFA',
    },
    itemLayBtn: {
        alignContent: 'flex-end'
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 24,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 100,
        height: 40
    },
    buttonStyle: {
        // alignSelf:'flex-start',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#009dff',
        padding: 5,
        // marginVertical: 10,
        width: 110,
        borderRadius: 12,
        elevation: 8,
        height: 30,
        marginLeft: 20
    },
    textBtn: {
        color: 'white',
        fontWeight: 'bold'
    },
})

export default ContactUpdate