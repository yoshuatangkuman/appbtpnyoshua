import React from 'react'
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux'
import { ClearContacts } from '../redux/actions/contact.action'
import { View, ScrollView, Text, StyleSheet, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { addContact, getContacts } from '../service/contactlist.service'
import { launchImageLibrary } from 'react-native-image-picker'
import storage from '@react-native-firebase/storage';
import contactValidation from '../validation/contactValidation';
import { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome'
import Photo from '../components/atoms/Photo'
import { AddContact } from '../redux/actions/contact.action';
import FormInputMolecule from '../components/molecules/FormInputMolecule';
import FormInputPhotoMolecule from '../components/molecules/FormInputPhotoMolecule';
import FormButton from '../components/atoms/FormButton';
import ContactStyle from '../assets/styles/ContactStyle';
import FormHeaderMolecule from '../components/molecules/FormHeaderMolecule';

function ContactAdd() {

    const dispatch = useDispatch()
    const navigate = useNavigation()

    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [age, setAge] = useState(0)
    const [imageurl, setImageurl] = useState()
    const [ageText, setAgeText] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [isError, setIsError] = useState(false)

    const error = useSelector(state => state.ContactReducer.error)
    const loading = useSelector(state => state.ContactReducer.loading)

    function handleFirstName(text) {
        text = text.replace(/ /g, '')
        setFirstname(text)
    }

    function handleLastName(text) {
        text = text.replace(/ /g, '')
        setLastname(text)
    }

    function handleAge(text) {
        text = text.replace(/[^0-9]/g, '')
        setAgeText(text)
        setAge(parseInt(text))
    }

    useEffect(()=> {
        if(isError){
            if(error){
                setIsLoading(false)
                ToastAndroid.show(error, ToastAndroid.LONG)
            }else{
                ToastAndroid.show("Contact Added", ToastAndroid.LONG)
                navigate.navigate("contact_list")
            }
        }else{
            setIsError(true)
        }
    }, [error, loading])

    function chooseFile() {
        setIsLoading(true)
        launchImageLibrary({}, async function (res) {
            if (res && res.assets) {
                const asset = res.assets[0]
                const reference = storage().ref(`/photos/${asset.fileName}`);

                await reference.putFile(asset.uri);
                const url = await reference.getDownloadURL();
                setImageurl(url)
                setIsLoading(false)
            } else {
                if (imageurl == "") {
                    setImageurl("N/A")
                }
                setIsLoading(false)
            }

        })
    }


    function validation() {
        setIsLoading(true)
        let messageError = contactValidation(firstname, lastname, age, imageurl)
        if (messageError == '') {
            navigatePageList()
        } else {
            ToastAndroid.show(messageError, ToastAndroid.LONG);
            setIsLoading(false)
        }
    }

    function navigatePageList() {
        const newContact = {
            firstName: firstname,
            lastName: lastname,
            age: age,
            photo: imageurl
        }

        console.log("newcontact screen===", newContact)

        // addContact(newContact).then(res => {
        //     if (res.message === "contact saved") {
        //         ToastAndroid.show(res.message, ToastAndroid.LONG)
        //         dispatch(ClearContacts())
        //         navigate.navigate("contact_list")
        //     }
        // }, (error) => {
        //     ToastAndroid.show(error, ToastAndroid.LONG)
        //     setIsLoading(false)
        // })
        dispatch(AddContact(newContact))
        

    }

    function navigatePageBack(){
        navigate.navigate("contact_list")
    }

    return (
        <ScrollView>
            <View style={ContactStyle.mainForm}>
                <FormHeaderMolecule label="Add Contact" name="angle-left" onPress={navigatePageBack}/>
                <View style={ContactStyle.loginFormBox}>
                    <FormInputMolecule label="First Name" value={firstname} placeholder="Insert First Name" onChangeText={handleFirstName}/>
                    <FormInputMolecule label="Last Name" value={lastname} placeholder="Insert Last Name" onChangeText={handleLastName}/>
                    <FormInputMolecule label="Age" keyboardType='numeric' value={ageText} placeholder="Insert Age" onChangeText={handleAge}/>
                    <FormInputPhotoMolecule label="Photo" onPress={() => chooseFile('photo')} imageurl={imageurl} isLoading={isLoading} />
                    {isLoading &&
                        <FormButton onPress={() => validation()} disabled={true}/>
                    }
                    {isLoading == false &&
                        <FormButton onPress={() => validation()} disabled={false}/>
                    }
                </View>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    photoBox: {
        alignSelf:'center',
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center',
        borderWidth:1
    }, 
    labelLoad: {
        width: 300,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    textStyle: {
        color: 'white'
    },
    image: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center'
    },
    imageLoad: {
        width: 150,
        height: 150,
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#009dff',
        alignSelf:'center',
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center'
    },
    mainForm: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    topNavBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        alignContent: 'center',
        width: '100%',
        // borderWidth:1,
        height:80
    },
    textTitlePageForm: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    iconForm: {
        color: 'white',
        alignSelf: 'center',
        marginLeft:20,
        marginRight:99,
    },
    mainBox: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24
    },
    loginFormBox: {

        height: 600,
        width: '100%',
        padding: 4,
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: 'white',
        borderTopLeftRadius: 25,
        borderTopRightRadius: 25
    },
    fieldBox: {
        alignItems: 'flex-start',
        width: 300
    },
    label: {
        marginVertical: 12,

    },
    labelPhoto: {
        marginVertical: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    label2: {
        color: '#009dff',
        fontWeight: 'bold',
        fontSize: 15
    },
    textInputField: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'flex-start',
        width: 300,
        borderWidth: 1,
        borderColor: '#009dff',
        borderRadius: 12
    },
    textInputFieldPhoto: {
        backgroundColor: 'white',
        color: 'black',
        alignItems: 'center',
        width: 300,
    },
    textMain: {
        color: '#8F98A9',
    },
    textHeader: {
        color: '#009dff',
        fontWeight: 'bold'
    },
    itemContent: {
        alignContent: 'flex-start',
    },
    itemList: {
        marginBottom: 24,
        marginHorizontal: 24,
        padding: 16,
        color: '#8F98A9',
        borderRadius: 12,
        backgroundColor: '#FAFAFA',
    },
    itemLayBtn: {
        alignContent: 'flex-end'
    },
    btn: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 24,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 100,
        height: 40
    },
    buttonStyle: {
        // alignSelf:'flex-start',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#009dff',
        padding: 5,
        // marginVertical: 10,
        width: 110,
        borderRadius: 12,
        elevation: 8,
        height: 30,
        marginLeft: 20
    },
    textBtn: {
        color: 'white',
        fontWeight: 'bold'
    },
})

export default ContactAdd