import React, { useCallback } from 'react'
import { useRoute, useNavigation, useFocusEffect } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux'
import ContactReducer from '../redux/reducers/contact.reducer'
import { InitiateContacts } from '../redux/actions/contact.action'
import { View, FlatList, Text, StyleSheet, Button, ToastAndroid, TextInput, TouchableOpacity, Image, ActivityIndicator } from 'react-native'
import { getContactDetail } from '../service/contactlist.service'
import { useState } from 'react';
import Icon from 'react-native-vector-icons/FontAwesome'

function ContactDetail() {

    const route = useRoute()
    const navigate = useNavigation()

    const [id, setId] = useState('')
    const [firstname, setFirstname] = useState('')
    const [lastname, setLastname] = useState('')
    const [age, setAge] = useState(0)
    const [imageurl, setImageurl] = useState()
    const [isLoading, setIsLoading] = useState(false)

    useFocusEffect(useCallback(() => {
        setIsLoading(true)
        getContactDetail(route.params.id).then(res => {
            if (res.message) {

                setId(res.data.id)
                setFirstname(res.data.firstName)
                setLastname(res.data.lastName)
                setAge(res.data.age)
                setImageurl(res.data.photo)
                setIsLoading(false)
            }
        }, (error) => {
            navigate.navigate("contact_list")
            ToastAndroid.show(error, ToastAndroid.LONG)
        })

    }, [route.params]))

    function navigatePageBack(){
        navigate.navigate("contact_list")
    }


    return (
        <View style={styles.main}>
            {isLoading && <ActivityIndicator style={{alignSelf:'center'}} size="large" color="white" />}
            {isLoading == false &&
                <View style={styles.main}>
                    <View style={styles.topNavBar}>
                        <Icon style={styles.icon} name="angle-left" size={30} onPress={navigatePageBack}/>
                        <Text style={styles.textTitlePage}>Contact Detail</Text>
                    </View>
                    <View style={styles.mainBox}>
                        <View style={styles.itemList}>

                            <View>
                                {imageurl != "N/A"
                                    ?
                                    <Image
                                        style={styles.image}
                                        source={{
                                            uri: imageurl,
                                        }}
                                    />
                                    :
                                    <Image
                                        style={styles.image}
                                        source={require('../images/noimage.png')}
                                    />

                                }
                            </View>
                            <View style={styles.itemContent}>
                                <Text style={styles.textHeader}>Name : {firstname} {lastname}</Text>
                                <Text style={styles.textMain}>Age :{age}</Text>
                            </View>

                        </View>
                    </View>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    itemList: {
        borderWidth:1,
        borderColor:'#009dff',
        marginBottom: 24,
        marginHorizontal: 24,
        padding: 16,
        color: '#8F98A9',
        borderRadius: 12,
        backgroundColor: '#F8FCFF',
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf:'center',
        width:'85%',
        maxWidth:350,
        alignSelf:'center'
    },
    image: {
        width: 100,
        height: 100,
        borderRadius: 25,
        marginRight: 20
    },
    main: {
        backgroundColor: '#009dff',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        width:'100%'
    },
    topNavBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        alignContent: 'center',
        width: '100%',
        height:80
    },
    textTitlePage: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    icon: {
        color: 'white',
        alignSelf: 'center',
        marginLeft:20,
        marginRight:92,
    },
    mainBox: {
        flex: 1,
        backgroundColor: 'white',
        width: '100%',
        borderTopLeftRadius: 36,
        borderTopRightRadius: 36,
        paddingTop: 24,
        
    },
    textMain: {
        color: '#8F98A9',
    },
    textHeader: {
        color: '#009dff',
        fontWeight: 'bold'
    },
    itemContent: {
        alignContent: 'flex-start',
    },
    itemLayBtn: {
        alignContent: 'flex-end',
        marginBottom: 20
    },
    btnAdd: {
        alignSelf: 'center',
        marginVertical: 12,
        elevation: 8,
        backgroundColor: "#009dff",
        borderRadius: 12,
        paddingVertical: 8,
        paddingHorizontal: 8,
        width: 100,
        height: 40
    },
    textBtnAdd: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold'
    },
    btnEdit: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#a1dbff",
        marginRight: 12,
        borderRadius: 2,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnEditText: {
        fontSize: 12,
        color: "#009dff",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
    btnDel: {
        alignSelf: 'flex-end',
        elevation: 1,
        backgroundColor: "#730202",
        marginRight: 12,
        borderRadius: 2,
        paddingVertical: 8,
        paddingHorizontal: 8,
        height: 35,
        width: 70
    },
    btnDelText: {
        fontSize: 12,
        color: "white",
        alignSelf: "center",
        textTransform: "uppercase",
        fontWeight: 'bold'
    },
})

export default ContactDetail