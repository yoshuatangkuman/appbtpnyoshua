/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import contactValidation from '../src/validation/contactValidation';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

test('Contact validation', () => {
  expect(contactValidation("yoshua","daniel",20,"photo")).toEqual("")
  expect(contactValidation("","asdaasd",20,"photo")).toEqual("Firstname can't be empty")
  expect(contactValidation("asdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa","asdasd",20,"photo")).toEqual("Firstname must be less than or equal to 30 characters")
  expect(contactValidation("asdasd","",20,"photo")).toEqual("Lastname can't be empty")
  expect(contactValidation("asdasd","asdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",20,"photo")).toEqual("Lastname must be less than or equal to 30 characters")
  expect(contactValidation("asdasd","asdasd",0,"photo")).toEqual("Age must be larger than or equal to 1")
  expect(contactValidation("asdasd","asdasd",222,"photo")).toEqual("Age must be less than or equal to 200")
  expect(contactValidation("asdasd","asdasd",100,"")).toEqual("Photo is not allowed to be empty")
  })
